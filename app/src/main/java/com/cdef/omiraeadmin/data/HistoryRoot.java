package com.cdef.omiraeadmin.data;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kimjinmo on 2017. 8. 28..
 */
@IgnoreExtraProperties
public class HistoryRoot implements Serializable {

    public ArrayList<History> time;

    public HistoryRoot()
    {

    }
}
