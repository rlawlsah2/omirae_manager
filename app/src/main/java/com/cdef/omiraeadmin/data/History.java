package com.cdef.omiraeadmin.data;

import android.support.annotation.NonNull;

import com.cdef.omiraeadmin.data.OrderedMenu;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kimjinmo on 2017. 8. 28..
 */
@IgnoreExtraProperties
public class History implements Serializable {

    public long state;
    public String address;
    public String time;
    public String phone;
    public String memo;
    public long delivery;
    public long deliveryFee;

    //    public Order history;
    @NonNull
    public ArrayList<OrderedMenu> menu;

    public History()
    {

    }
}
