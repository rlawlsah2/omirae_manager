package com.cdef.omiraeadmin;

import android.app.Application;
import android.content.Context;

//import android.support.multidex.MultiDex;


/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class OmiraeApplication extends Application {


//    private FirebaseDatabase mDatabase;
//    private DatabaseReference mDBRef;

    public static Context mContext;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.mContext = this;

//        if(mDatabase == null)
//        {
//            this.mDatabase = FirebaseDatabase.getInstance();
//        }

    }
}
