package com.cdef.omiraeadmin.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.cdef.omiraeadmin.activity.HistoryDetailActivity;
import com.cdef.omiraeadmin.activity.HistoryListActivity;
import com.cdef.omiraeadmin.data.History;


/**
 * Created by kimjinmo on 2016. 11. 16..
 */

public class NavigationUtils {

    public static void goHistoryListActivity(Context context)
    {
        Intent intent = new Intent(context, HistoryListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(0,0);

    }
    public static void goHistoryDetailActivity(Context context, History history)
    {
        Intent intent = new Intent(context, HistoryDetailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("history", history);
        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(0,0);

    }
}
