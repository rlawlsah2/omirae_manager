package com.cdef.omiraeadmin.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class Utils {

    public Utils()
    {

    }

    public String phoneWithDash(String phone)
    {
        switch (phone.length())
        {
            case 11:
                return phone.substring(0,3) + "-" + phone.substring(3,7) + "-" + phone.substring(7,11);
            case 10:
                return phone.substring(0,3) + "-" + phone.substring(3,6) + "-" + phone.substring(6,10);
            default:
                return phone;
        }
    }

    /**
     * 1000단위 콤마
     * **/
    public String setComma(long data)
    {
        int result = Integer.parseInt(data + "");
        return new java.text.DecimalFormat("#,###").format(result);
    }


    /**
     * dp -> pixel 변환
     * @param context
     * @param dp dp값
     * return int 픽셀로 변환된 값
     * ***/
    public int dpToPx(Context context, int dp) {
        Resources resources = context.getResources();

        DisplayMetrics metrics = resources.getDisplayMetrics();

        float px = dp * (metrics.densityDpi / 160f);

        return (int) px;
    }
    /**
     * pixel -> dp 변환
     * @param context
     * @param px px 값
     * return int dp로 변환된 값
     * ***/
    public int pxToDp(Context context, int px) {
        Resources resources = context.getResources();

        DisplayMetrics metrics = resources.getDisplayMetrics();

        float dp = px / (metrics.densityDpi / 160f);

        return (int) dp;

    }


    public String getCurrentTime()
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss");
        String sDate = formatter2.format(cal.getTime()); // for file-name


        /**
         * firebase 때문에 9999999999에 뺀값을 리턴해준다.
         * **/

        long corrector = Long.parseLong("99999999999999");
        long oDate = Long.parseLong(sDate);

        return String.valueOf((corrector - oDate));
    }

    public static String calcDate(String input)
    {
        /**
         * firebase 때문에 9999999999에 뺀값을 리턴해준다.
         * **/

        long corrector = Long.parseLong("99999999999999");
        long oDate = Long.parseLong(input);

        return String.valueOf((corrector - oDate));
    }



    public static String convertDate(String newFormat, String time)
    {
        SimpleDateFormat currentTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat currentTimeFormat_day = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat currentTimeFormat_hour = new SimpleDateFormat("HH시 mm분");
        SimpleDateFormat returnTimeFormat = new SimpleDateFormat(newFormat);



        try {
            ///오늘 날짜 표현식은 좀 다르게
            Date today = new Date();
            String sToday = currentTimeFormat_day.format(today).toString();
            Date inputTime = currentTimeFormat.parse(time);
            String inputTime_day = currentTimeFormat_day.format(inputTime);


            LogUtil.d("날자 표현 sToday: " + sToday);
            LogUtil.d("날자 표현 sInputDat: " + inputTime_day);

            if(sToday.equals(inputTime_day))
            {
                return "오늘 " + currentTimeFormat_hour.format(inputTime);
            }


            String result = returnTimeFormat.format(inputTime);
            return result;

        } catch (ParseException e) {
            e.printStackTrace();
            return time;
        }

    }


}
