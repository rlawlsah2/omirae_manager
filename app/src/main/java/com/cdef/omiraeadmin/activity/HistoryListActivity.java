package com.cdef.omiraeadmin.activity;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.cdef.omiraeadmin.R;
import com.cdef.omiraeadmin.adapter.HistoryListItem;
import com.cdef.omiraeadmin.customView.MessageDialogBuilder;
import com.cdef.omiraeadmin.data.Default;
import com.cdef.omiraeadmin.data.History;
import com.cdef.omiraeadmin.util.LogUtil;
import com.cdef.omiraeadmin.util.NavigationUtils;
import com.cdef.omiraeadmin.util.Utils;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


/***
 *  장바구니/결제 페이지를 보여준다.
 *
 * **/
public class HistoryListActivity extends BaseActivity {
    Utils mUtils = new Utils();
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    public final int REQUEST_PHONENUMBER = 98;
    public final int REQUEST_ADDRESS = 99;

    public final int REQUEST_OK = 1;
    public final int REQUEST_CANCEL = 0;
    @BindView(R.id.listview)
    ListView mListview;

    FirebaseListAdapter mAdapter;
    @BindView(R.id.button_state)
    Button mButtonState;
    @OnClick(R.id.button_state)
    void changeState(View view)
    {
        if(mDefault != null) {
            MessageDialogBuilder.getInstance(this)
                    .setTitle("영업 상태 변경")
                    .setContent(
                            (mDefault.open == 0) ? "영업을 개시하시겠습니까?" : "영업을 종료하시겠습니까?"
                    )
                    .setDefaultButtons(view1 -> {

                        FirebaseDatabase.getInstance().getReference().child("default").child("open").runTransaction(new Transaction.Handler() {
                            @Override
                            public Transaction.Result doTransaction(MutableData mutableData) {
                                mutableData.setValue((mDefault.open == 0) ? 1 : 0);
                                return Transaction.success(mutableData);
                            }

                            @Override
                            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                                mDefault.open = (dataSnapshot.getValue(Long.class));
                                checkState();
                                MessageDialogBuilder.getInstance(mContext).dismiss();

                            }
                        });
                    })
                    .complete();
        }






    }
    @BindView(R.id.layout_top)
    RelativeLayout layoutTop;
    @BindView(R.id.layout_state)
    RelativeLayout mLayoutState;
    @OnClick(R.id.layout_state)
    void clickState(View v)
    {

    }

    private CompositeSubscription subscriptions;
    Default mDefault;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_list);
        ButterKnife.bind(this);


        /**
         * 초기값 셋팅
         * **/
        FirebaseDatabase.getInstance().getReference().child("default")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        LogUtil.d("시스템 정보 : " + dataSnapshot);
                        if (dataSnapshot != null) {
                            mDefault = dataSnapshot.getValue(Default.class);
                            checkState();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


//        Observable.just(check()).

        subscriptions = new CompositeSubscription();

        Subscription subscription = Observable.interval(0, 1, TimeUnit.MINUTES)
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .map(aLong -> false)
                .repeat()
                .takeUntil(result -> check())
                .subscribe(
                        result -> {
                            Log.d("kk9999", "반복문 안에 들어옴 : " + result);

                        },
                        error -> {
                            Log.d("kk9999", "반복문 error 안에 들어옴 : " + error);

                        },
                        () -> {
                            Log.d("kk9999", "반복문 complete 안에 들어옴 : ");
                            finish();
                        }


                );


        this.subscriptions.add(subscription);


    }

    @Override
    protected void onResume() {
        super.onResume();

        String token = FirebaseInstanceId.getInstance().getToken();
        LogUtil.d("관리자 fcm token : " + token);
        if (token != null)
            FirebaseDatabase.getInstance().getReference("manager").child(token).setValue(1);

//        ///폰 번호 가져오기. 미 승인시 뒤로가기
//        getPhoneNumber();
        if (mAdapter == null) {
            mAdapter = new HistoryListItem(mContext, History.class, R.layout.layout_history_item, this.database.getReference("order_manager").orderByChild("state"));
            mListview.setAdapter(mAdapter);
            mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    NavigationUtils.goHistoryDetailActivity(mContext, ((History) mAdapter.getItem(i))
                    );
                }
            });

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PHONENUMBER: {
                getPhoneNumberProcess();
                break;
            }
        }
    }

    /**
     * 유저의 휴대폰 번호를 불러옴
     ***/
    private void getPhoneNumber() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            MessageDialogBuilder.getInstance(this)
                    .setTitle("알림")
                    .setContent("회원님의 주문내역은 해당 휴대전화 번호를 기반으로 확인하기때문에 권한 승인을 해주셔야 합니다.")
                    .setDefaultButtons(view -> {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_PHONENUMBER);
                        MessageDialogBuilder.getInstance(this).dismiss();
                    })
                    .complete();

        } else {
            //이미 승인완료 따라서 번호 가져올것.
            getPhoneNumberProcess();
        }
    }

    private String getPhoneNumberProcess() {
        String myNumber = null;
        TelephonyManager mgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        try {
            myNumber = mgr.getLine1Number();
            myNumber = myNumber.replace("+82", "0");
            myNumber = myNumber.replace("-", "");
            setFirebaseDB(myNumber);
            return myNumber;
        } catch (Exception e) {
            return myNumber;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.subscriptions != null) {
            this.subscriptions.clear();
            this.subscriptions.unsubscribe();
        }
        if (mAdapter != null)
            this.mAdapter.cleanup();
    }

    private void setFirebaseDB(String phone) {
//        if (phone != null) {
//            if(mAdapter == null)
//            {
//                mAdapter = new HistoryListItem(mContext, History.class, R.layout.layout_history_item, this.database.getReference("order").orderByChild("state").limitToFirst(100));
//                mListview.setAdapter(mAdapter);
//                mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                        NavigationUtils.goHistoryDetailActivity(mContext, ((History)mAdapter.getItem(i))
//                        );
//                    }
//                });
//
//            }
//
//
//        } else {
//            MessageDialogBuilder.getInstance(this)
//                    .setTitle("알림")
//                    .setContent("해당 기기의 전화번호를 불러올 수 없어 목록을 가져올 수 없습니다.")
//                    .setDefaultButtons(view -> {
//                        MessageDialogBuilder.getInstance(this).dismiss();
//                        finish();
//                    })
//                    .complete();
//        }
    }


    private boolean check() {

        ///조건식 : 주문을 한번도 확인 안한 얘들만 처리할것.
        FirebaseDatabase.getInstance().getReference("order_manager").orderByChild("state").equalTo(1).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LogUtil.d("무한 루프 체크 : " + dataSnapshot);
                if (dataSnapshot.getChildrenCount() > 0) {
                    ///알림
                    Intent intent = new Intent(mContext, HistoryListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0 /* Request code */, intent,
                            PendingIntent.FLAG_ONE_SHOT);


                    Uri soundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.master2);

                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("주문을 확인해주세요")
                            .setAutoCancel(true)
                            .setSound(soundUri)
                            .setContentIntent(pendingIntent);

                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    notificationManager.notify(99099 /* ID of notification */, notificationBuilder.build());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return false;
    }

    /*
    * 현재 상태를 기반으로 뷰를 셋팅
    * **/
    private void checkState()
    {

        if(mDefault != null)
        {

            LogUtil.d("checkState : " + mDefault.open);
            if(mDefault.open == 0)  //닫은상태
            {
                mButtonState.setBackgroundResource(R.drawable.selector_btn_clickeffect_graydefault);
                mButtonState.setText("영업종료");
                mLayoutState.setVisibility(View.VISIBLE);
            }
            else
            {
                mButtonState.setBackgroundResource(R.drawable.selector_btn_clickeffect_bright);
                mButtonState.setText(" 영업중 ");
                mLayoutState.setVisibility(View.GONE);

            }
        }
    }
}
