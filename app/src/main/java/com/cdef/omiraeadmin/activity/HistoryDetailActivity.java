package com.cdef.omiraeadmin.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cdef.omiraeadmin.R;
import com.cdef.omiraeadmin.customView.OrderedMenuListItem;
import com.cdef.omiraeadmin.data.History;
import com.cdef.omiraeadmin.data.OrderedMenu;
import com.cdef.omiraeadmin.util.LogUtil;
import com.cdef.omiraeadmin.util.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/***
 *  주문내역 상세내용을 보여준다.
 * **/
public class HistoryDetailActivity extends BaseActivity {
    public final int REQUEST_PHONENUMBER = 98;

    FirebaseDatabase database;// = FirebaseDatabase.getInstance();
    DatabaseReference myRef;// = database.getReference("cart");
    @BindView(R.id.layout_order_list)
    LinearLayout mLayoutOrderList;
    @BindView(R.id.text_price)
    TextView mTextPrice;
    @BindView(R.id.text_address)
    TextView mTextAddress;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;
    @BindView(R.id.button_order)
    Button mButtonOrder;
    @BindView(R.id.text_phone)
    TextView mTextPhone;
    @BindView(R.id.text_memo)
    TextView mTextMemo;
    @BindView(R.id.text_delivery_fee)
    TextView mTextDeliveryFee;

    @OnClick(R.id.button_order)
    void changeState(View v) {
        switch ((int) mHistory.state) {
            case 1:
                FirebaseDatabase.getInstance().getReference().child("order_manager").child(mHistory.time).child("state").runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        mutableData.setValue(0);
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                        mHistory.state = dataSnapshot.getValue(Long.class);
                        viewSetting();
                    }
                });
                break;
            case 0:
                FirebaseDatabase.getInstance().getReference().child("order_manager").child(mHistory.time).child("state").runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        mutableData.setValue(2);

                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                        mHistory.state = dataSnapshot.getValue(Long.class);
                        viewSetting();

                    }
                });
                break;
            case 2:
                Toast.makeText(mContext, "배달 완료되었습니다.", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @BindView(R.id.text_state)
    TextView mTextState;
    @BindView(R.id.text_state_sub)
    TextView mTextStateSub;


    private History mHistory;
    private Utils mUtils = new Utils();
    private long mTotalPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_datail);
        ButterKnife.bind(this);
        this.mContext = this;


        /**
         * Intent로 넘겨받은 데이터를 파싱해야 함
         * **/

        Intent bundle = getIntent();
        LogUtil.d("주문내역 상세보기 bundle : " + bundle);
//        LogUtil.bundle2string(bundle.getSerializableExtra("history"));

        if (bundle != null) {
            mHistory = (History) bundle.getSerializableExtra("history");
            LogUtil.d("주문내역 상세보기 mHistory : " + mHistory);
            LogUtil.d("주문내역 상세보기 mHistory.state : " + mHistory.state);


            ///셋팅
            this.mTextAddress.setText(mHistory.address);
            this.mTextPhone.setText(mUtils.phoneWithDash(mHistory.phone));
            this.mTextMemo.setText(mHistory.memo);

            viewSetting();

            switch ((int) mHistory.delivery) {

                case 0:
                    this.mTextAddress.setText(mHistory.address);
                    this.mTextDeliveryFee.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    this.mTextAddress.setText("방문 포장");
                    this.mTextDeliveryFee.setVisibility(View.GONE);
                    break;
                case 2:
                    this.mTextAddress.setText("매장 식사");
                    this.mTextDeliveryFee.setVisibility(View.GONE);
                    break;
            }

            for (OrderedMenu menu : mHistory.menu) {
                LogUtil.d("@넘겨받은 데이터를 확인해보자 : " + menu.title);
                LogUtil.d("넘겨받은 데이터를 확인해보자 : " + menu.count);
                LogUtil.d("넘겨받은 데이터를 확인해보자 : " + menu.description);
                LogUtil.d("넘겨받은 데이터를 확인해보자 : " + menu.price);
                OrderedMenuListItem newItem = new OrderedMenuListItem(mContext);
                newItem.setData(menu.title, menu.count, mUtils.setComma(menu.price) + "원");
                this.mLayoutOrderList.addView(newItem);
                this.mTotalPrice += menu.price * menu.count;
            }

            if (mHistory.delivery == 0) {
                this.mTotalPrice += mHistory.deliveryFee;
                this.mTextPrice.setText("* " + mUtils.setComma(this.mTotalPrice) + "원");
            } else
                this.mTextPrice.setText(mUtils.setComma(this.mTotalPrice) + "원");


        }

    }

    private void viewSetting() {
        switch ((int) mHistory.state) {

            case 1:
                this.mButtonOrder.setText("주문 접수하기");
                this.mTextState.setText("주문 접수중");
                this.mTextState.setTextColor(Color.parseColor("#DF4D4D"));
                break;
            case 0:
                this.mButtonOrder.setText("배달 시작하기");
                this.mTextState.setText("주문 확인 및 처리중");
                this.mTextState.setTextColor(Color.parseColor("#DF4D4D"));
                break;
            case 2:
                this.mButtonOrder.setText("배달 완료");
                this.mTextState.setText("배달 완료");
                this.mTextState.setTextColor(Color.parseColor("#000000"));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (resultCode == 0) {
                //그냥 취소인경우
            } else if (resultCode == 1) {
                myRef.removeValue();
                finish();
            }
        }
    }

}
