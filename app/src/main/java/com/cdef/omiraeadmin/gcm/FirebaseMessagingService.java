package com.cdef.omiraeadmin.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.cdef.omiraeadmin.R;
import com.cdef.omiraeadmin.activity.HistoryListActivity;
import com.cdef.omiraeadmin.util.LogUtil;
import com.google.firebase.messaging.RemoteMessage;


public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    private static final String TAG = "FirebaseMsgService";

    @Override
    public void onCreate() {
        super.onCreate();




    }

    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        LogUtil.d("fcm 들어온다1 : " + remoteMessage);
        LogUtil.d("fcm 들어온다2 : " + remoteMessage.getMessageId());
        LogUtil.d("fcm 들어온다3 : " + remoteMessage.getData());
        LogUtil.d("fcm 들어온다4 : " + remoteMessage.getFrom());
        LogUtil.d("fcm 들어온다5 : " + remoteMessage.getNotification());
        for(String item : remoteMessage.getData().keySet())
        {
            LogUtil.d("fcm 들어온다5 : " + item);

        }
        //추가한것
        sendNotification(remoteMessage.getData().get("message"));
    }


    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, HistoryListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);


        Uri soundUri= Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.master);

//        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(messageBody + "주문이 들어왔습니다.")
                .setTicker(messageBody + "주문이 들어왔습니다.")
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(99099 /* ID of notification */, notificationBuilder.build());
    }

}

